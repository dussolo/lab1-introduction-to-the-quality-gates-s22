FROM openjdk:11.0.14-jre-slim-bullseye

ARG APP_VERSION=0.0.1

RUN useradd -Umd /opt/app app-runner

USER app-runner
WORKDIR /opt/app

COPY target/main-${APP_VERSION}-SNAPSHOT.jar app.jar

CMD java -jar app.jar

EXPOSE 8080