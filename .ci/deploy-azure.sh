#!/usr/bin/env bash

pushd "${APP_PATH}" || (
  echo "ERROR Cannot cd to the ${APP_PATH}"
  exit 1
)

[[ -f docker-compose.yml ]] && rm docker-compose.yml

APP_IMAGE_NAME=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} \
  envsubst <"${COMPOSE_TEMPLATE}" | docker stack deploy -c - "${APP_STACK}"

popd || (
  echo "WARN Cannot cd back"
  exit 0
)
