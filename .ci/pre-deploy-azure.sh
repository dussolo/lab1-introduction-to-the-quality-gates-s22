#!/usr/bin/env bash

[[ -d "${APP_PATH}" ]] && sudo rm -rf "${APP_PATH}"

sudo mkdir -p "${APP_PATH}"
sudo chown -R "${AZURE_SSH_USER}" "${APP_PATH}"
